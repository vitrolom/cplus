﻿/*
Реалізувати класичний паттерн observer
*/

#include <iostream>
#include <vector>
#include <string>

class Observer {
	public:
		virtual void update(const std::string& message) = 0;
};

class Player : public Observer {
	private:
		std::string name;

	public:
		Player(const std::string& playerName) : name(playerName) {}

		void update(const std::string& message) override {
			std::cout << "player " << name << "get message " << message << std::endl;
		}
};

class Game {
	private: 
		std::vector<Observer*> observers;
		std::string lastEvent;
	public:
		void addObserver(Observer* observer) {
			observers.push_back(observer);
		}
		void removeObserver(Observer* observer) {
			auto it = std::find(observers.begin(), observers.end(), observer);
			if (it != observers.end()) {
				observers.erase(it);
			}
		}
		void notifyObservers() {
			for (Observer* observer : observers) {
				observer->update(lastEvent);
			}
		}
		void gameEvent(const std::string& event) {
			lastEvent = event;
			notifyObservers();
		}
};

int main() {
	Game game;

	Player player1("player 1");
	Player player2("player 2");

	game.addObserver(&player1);
	game.addObserver(&player2);

	game.gameEvent("player 1 get 10 points!");
	game.gameEvent("player 2 get new artefact!");

	game.removeObserver(&player2);

	game.gameEvent("player 1 up his level");

	return 0;

}


/*
Створити базовий клас Персонажа Character із методами
- void SetWeapon(Weapon* weapon); дає вепон персонажу

- Weapon* GetWeapon() const; дає змогу дізнатися, що за вепон у персонажа

Створити базовий клас Weapon із двома абстрактними методами
- float GetDamage() = 0; повертає значення шкоди вепона

- string GetName() = 0; повертає назву вепона

Створити класи-нащадки різних видів вепонів
Завдання із зірочкою:
- Реалізувати класс DamageModifier в якому є функція калькуляції урона

* float CalculateDamage(float CurrentHealth, float Damage) = 0;

- Створити нащадків цього класу із різною логікою модифікації урона. Наприклад:

* MultiplicationDamageModifier(float multiplicator) – клас який множить шкоду

* AdditionDamageModifier(float addition) – клас який додає/зменшує  шкоду від вепона (але не менше нуля)

* ParityDamageModifier(float multiplicator) – клас який множить тількі парні запроси на зміну демеджа

- Додати в клас персонажа функцію

* float GetModifiedDamage(); повертає модифіковану шкоду від зброї

- Створювати об’єкти класів вище, і динамічно назначати їх в персонажі

* Тобто якщо я захочу змінити демедж модифаєр після якогось інпуту, то в мене буде змога це зробити
* 
* + перепис з використанням розумних вказівників
*/

/*
#include <iostream>
#include <string>
#include <memory>

using namespace std;

class Weapon {
public:
	virtual float GetDamage() const = 0;
	virtual string GetName() const = 0;
};

class DamageModifier {
public:
	virtual float CalculateDamage(float CurrentHealth, float Damage) const = 0;
};

class Character {
private:
	unique_ptr<Weapon> weapon;
	unique_ptr<DamageModifier> modifier;
public:
	Character() {}

	void SetWeapon(unique_ptr<Weapon> weapon) {
		this->weapon = move(weapon);
	}

	void SetDamageModifier(unique_ptr<DamageModifier> modifier) {
		this->modifier = move(modifier);
	}

	Weapon* GetWeapon() const {
		return weapon.get();
	}

	float GetModifiedDamage(float CurrentHealth) const {
		if (weapon && modifier) {
			float damage = weapon->GetDamage();
			return modifier->CalculateDamage(CurrentHealth, damage);
		}
		return 0.0f;
	}
};

class MultiplicationDamageModifier : public DamageModifier {
private:
	float multiplicator;
public:
	MultiplicationDamageModifier(float multiplicator) : multiplicator(multiplicator) {}

	float CalculateDamage(float CurrentHealth, float Damage) const override {
		return Damage * multiplicator;
	}
};

class AdditionDamageModifier : public DamageModifier {
private:
	float addition;
public:
	AdditionDamageModifier(float addition) : addition(addition) {}

	float CalculateDamage(float CurrentHealth, float Damage) const override {
		float modifiedDamage = Damage + addition;
		return (modifiedDamage > 0.0f) ? modifiedDamage : 0.0f;
	}
};

class ParityDamageModifier : public DamageModifier {
private:
	float multiplicator;
public:
	ParityDamageModifier(float multiplicator) : multiplicator(multiplicator) {}

	float CalculateDamage(float CurrentHealth, float Damage) const override {
		if (static_cast<int>(CurrentHealth) % 2 == 0) {
			return Damage * multiplicator;
		}
		else {
			return Damage;
		}
	}
};

// Клас Weapon1 - конкретна реалізація зброї
class Weapon1 : public Weapon {
public:
	float GetDamage() const override {
		return 10.0f; 
	}

	string GetName() const override {
		return "Weapon1"; 
	}
}; 

int main() {
	Character character;

	unique_ptr<Weapon> weapon = make_unique<Weapon1>();
	unique_ptr<DamageModifier> modifier = make_unique<MultiplicationDamageModifier>(1.5f);

	character.SetWeapon(move(weapon));
	character.SetDamageModifier(move(modifier));

	float modifiedDamage = character.GetModifiedDamage(100.0f); // Припустимо, що поточне здоров'я персонажа - 100
	cout << "Modified damage: " << modifiedDamage << endl;

	return 0;
}
*/

/*
Для симулятора міста розробіть клас Building із наступними змінними
- Id: int (повинен бути унікальний для всіх будівель)

- MaxAge: int – максимальна кількість років, яка ця будівля простоїть\

- Initial Cost: int – початкова вартість будівлі

В цього класса повинні бути наступні функції-члени:
- Int GetCost();

* Повертає поточну вартість будівлі. Із кожним роком вартість будівлі зменшується лінійно: 0 років – initial cost, MaxAge років – 0$, MaxAge/2 – InitialCost/2

- void ToAge(int years);

* Виклик цієї функції повинен зістарити будівлю на year років

* Якщо будівля зістарилася більше, ніж її максимальний вік, то вона повинна викликати приватну фукцію Destroy();

- void Destroy();

* Приватна функція, яка в косноль повідомить будівля із яким id була знищенна
*/

/*
#include <iostream>
#include <vector>

class Building {
private:
	static int nextId; 
	int id;
	int maxAge;
	int initialCost;
	int currentCost;

	void Destroy() {
		std::cout << "The building id " << id << " was destroy." << std::endl;
	}

public:
	Building(int age, int cost) : id(nextId++), maxAge(age), initialCost(cost), currentCost(cost) {}

	int GetCost() const {
		return currentCost;
	}

	void ToAge(int years) {
		if (years >= maxAge) {
			Destroy();
		}
		else {
			currentCost = initialCost - initialCost * years / (2 * maxAge);
		}
	}

	void AgeBuilding(int years) {
		ToAge(years);
	}
};

int Building::nextId = 1; 

int main() {
	std::vector<Building> buildings;

	buildings.push_back(Building(50, 100000)); 
	std::cout << "The init price of building: " << buildings[0].GetCost() << std::endl;

	buildings[0].AgeBuilding(25); 
	std::cout << "The building cost after 25 years: " << buildings[0].GetCost() << std::endl;

	buildings[0].AgeBuilding(51);
	return 0;
}
*/

/*

1.Зчитати з консолі в наступному форматі декілька команд:

Назва клану (string)
Ім’я гравця (string)
Сила гравця (float)

2.Зберегти данні в мапі, де ключем є назва клану

3.Розробити фунцію, яка повертає кількість гравців у клані за її назвою

Int GetPlayerCount(constr string& ClanName);
4.Розробити функцію, яка повертає результат поєдинку між кланами.

Int ClanFight(constr string& FirstClanName, const string& SecondClanName);
Функція повертає:
⁃ 0, якщо нічия

⁃ 1, якщо переміг перший клан

⁃ -1, якщо переміг другий клан

Умова перемоги: сумарна сила членів клану більша за сумарну силу іншого клан

*/

/*

#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

struct Player {
	string name;
	float strength;
};

int countPlayersInClan(const unordered_map<string, vector<Player>>& clanMap, const string& clanName) {
	if (clanMap.find(clanName) != clanMap.end()) {
		return clanMap.at(clanName).size();
	}
	else {
		return 0;
	}
}

int ClanFight(const string& firstClanName, const string& secondClanName, const unordered_map<string, vector<Player>>& clanMap) {
	int totalStrengthFirstClan = 0;
	int totalStrengthSecondClan = 0;

	for (const auto& player : clanMap.at(firstClanName)) {
		totalStrengthFirstClan += player.strength;
	}

	for (const auto& player : clanMap.at(secondClanName)) {
		totalStrengthSecondClan += player.strength;
	}

	if (totalStrengthFirstClan == totalStrengthSecondClan) {
		return 0;
	}
	else if (totalStrengthFirstClan > totalStrengthSecondClan) {
		return 1;
	}
	else {
		return -1;
	}
}

int main() {
	unordered_map<string, vector<Player>> clanMap;

	while (true) {
		string clanName, playerName;
		float playerStrength;

		cout << "add clan name or exit: ";
		getline(cin, clanName);

		if (clanName == "exit") {
			break;
		}

		cout << "name: ";
		getline(cin, playerName);

		cout << "power: ";
		cin >> playerStrength;

		cin.ignore();

		Player player;
		player.name = playerName;
		player.strength = playerStrength;

		if (clanMap.find(clanName) == clanMap.end()) {
			clanMap[clanName] = vector<Player>{ player };
		}
		else {
			clanMap[clanName].push_back(player);
		}
	}

	cout << "\nresults:\n";
	for (const auto& pair : clanMap) {
		cout << "clan: " << pair.first << endl;
		for (const auto& player : pair.second) {
			cout << "  player: " << player.name << ", power: " << player.strength << endl;
		}
	}

	cout << "writhe the clan name: ";
	string clanNameToCheck = "clan";
	cin >> clanNameToCheck;
	int playerCount = countPlayersInClan(clanMap, clanNameToCheck);
	cout << "how much there players in clan '" << clanNameToCheck << "': " << playerCount << endl;

	string firstClanName, secondClanName;
	cout << "the name of first clan: ";
	cin >> firstClanName;
	cout << "the name of second clan: ";
	cin >> secondClanName;

	int result = ClanFight(firstClanName, secondClanName, clanMap);

	if (result == 0) {
		cout << "peace" << endl;
	}
	else if (result == 1) {
		cout << "the victory of clan: '" << firstClanName << "'" << endl;
	}
	else {
		cout << "the victory of clan: '" << secondClanName << "'" << endl;
	}

	return 0;
}
// --------------- lesson 15
*/
/*
Створити свою структуру, в якій будуть наступні дані
- ID персонажа : int

- Клас персонажа : enum

- Сила близкьої (melee) атаки  : float

- Сила дальної (ranged) атаки : float

Створити функцію, яка буде приймати на вхід вектор даних структур, та повертати id найсильнішого персонажа.
- Найсильніший – сума melee та ranged атак найбільша.

Завдання із зірочкою:
- Розробити функцію, яка поверне найсильнішого в кожному класі.
*/
/*
#include <iostream>
#include <vector> 
using namespace std;

enum CharacterClass { Warrior, Mage, Archer };

struct Character {
	int id;                    
	CharacterClass characterClass;
	float meleeAttack;
	float rangedAttack;
};


int findStrongestCharacterId(const std::vector<Character>& characters) {
	float maxAttackSum = 0.0f;
	int strongestId = -1;

	for (const auto& character : characters) {
		float attackSum = character.meleeAttack + character.rangedAttack;

		if (attackSum > maxAttackSum) {
			maxAttackSum = attackSum;
			strongestId = character.id;
		}
	}

	return strongestId;
}

std::vector<int> findStrongestCharactersByClass(const std::vector<Character>& characters) {
	std::vector<float> maxAttackSumByClass = { 0.0f, 0.0f, 0.0f };
	std::vector<int> strongestIdsByClass = { -1, -1, -1 };

	for (const auto& character : characters) {
		float attackSum = character.meleeAttack + character.rangedAttack;

		if (attackSum > maxAttackSumByClass[character.characterClass]) {
			maxAttackSumByClass[character.characterClass] = attackSum;
			strongestIdsByClass[character.characterClass] = character.id;
		}
	}

	return strongestIdsByClass;
}

	
int main() {
	

	std::vector<Character> characters = {
		{1, Warrior, 10.5f, 5.2f},
		{2, Mage, 8.3f, 12.7f},
		{3, Archer, 15.2f, 7.9f},
		{4, Warrior, 12.1f, 8.6f},
		{5, Mage, 7.8f, 11.5f},
		{6, Archer, 16.7f, 9.3f}
	};

	int strongestId = findStrongestCharacterId(characters);
	if (strongestId != -1) {
		std::cout << "the most powerful player id is: " << strongestId << std::endl;
	}
	else {
		std::cout << "there are no players" << std::endl;
	}

	std::vector<int> strongestIdsByClass = findStrongestCharactersByClass(characters);
	std::cout << "the most powerful players by class are: " << std::endl;
	std::cout << "warrior: " << strongestIdsByClass[Warrior] << std::endl;
	std::cout << "mag: " << strongestIdsByClass[Mage] << std::endl;
	std::cout << "archer: " << strongestIdsByClass[Archer] << std::endl;

	return 0;
}

// --------------- lesson 14

/*
Написати програму, яка запитує і запам’ятовує числа, які дорівнюють кількості шкоди (або лікуванню, якщо число від’ємне).
Розробити для цих даних функції статистики:
Під якими номерами нам завдали найбільшу шкоду
Під якими номерами нам завдали нам найменше шкоди
Під якими номерами нам завдали лікування
Скільки всього нам нанесли шкоди/лікування (в параметр функції потрібно передати обраний варіант: лікування, або шкода)
Чи є хтось, хто нічого нам не зробив (значення дорівнює нулю)
*/

/*
#include <iostream>
#include <vector> 
#include <chrono>
#include <thread>
using namespace std;

enum typeOfChange {
	damage,
	healing,
	zero
};

void displaySplashScreen() {
	std::cout << "==============================================" << std::endl;
	std::cout << "            Welcome to Statistic         " << std::endl;
	std::cout << "==============================================" << std::endl;
	std::cout << std::endl;

	std::cout << "Loading ";
	std::string loadingBar = "|/-\\";
	for (int i = 0; i < 20; ++i) {
		std::cout << loadingBar[i % loadingBar.size()] << "\b";
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		std::cout.flush();
	}
	std::cout << std::endl << std::endl;
}

vector<size_t> getZeroIndices(const std::vector<float>& vec) {
	vector<size_t> zeroIndices; 

	for (size_t i = 0; i < vec.size(); ++i) {
		if (vec[i] == 0) {
			zeroIndices.push_back(i + 1);
		}
	}

	return zeroIndices; 
}

float sumNumbers(vector<float>& numbers, typeOfChange change) {
	float sum = 0.0f;

	for (float number : numbers) {
		if (change == healing) {
			if (number < 0) {
				sum += number;
			}
		} 
		else {
			if (number > 0) {
				sum += number;
			}
		}
	}

	return sum;
}

vector<size_t> getNegativeIndices(const vector<float>& vec) {
	vector<size_t> negativeIndices; 

	for (size_t i = 0; i < vec.size(); ++i) {
		if (vec[i] < 0) {
			
			negativeIndices.push_back(i + 1);
		}
	}

	return negativeIndices; 
}


float findBiggestPositiveIndex(const vector<float>& vec) {
	if (vec.empty()) {
		return 0.0f;
	}

	float maxVal = vec[0];
	size_t maxIndex = 0;

	for (size_t i = 1; i < vec.size(); ++i) {
		if (vec[i] > maxVal) {
			maxVal = vec[i];
			maxIndex = i;
		}
	}
	
	return maxIndex + 1;
}

size_t findSmallestPositiveIndex(const std::vector<float>& vec) {
	float smallestPositive = std::numeric_limits<float>::max(); 
	size_t smallestPositiveIndex = vec.size(); 


	for (size_t i = 0; i < vec.size(); ++i) {
		if (vec[i] > 0 && vec[i] < smallestPositive) {
			
			smallestPositive = vec[i];
			smallestPositiveIndex = i;
		}
	}

	return smallestPositiveIndex + 1; 
}


vector<float> damageHealingBox(int tryNum) {
	vector<float> damageHealingArr;

	int count = 0;
	while (count < tryNum) {
		float value; 
		cout << "Enter a damage (+ value) or healing (- value): ";
		cin >> value;
		damageHealingArr.push_back(value);
		count++;
	}

	return damageHealingArr;
}


int main() {
	
	int trying = 0;
	float max = 0;
	float min = numeric_limits<float>::max();

	bool damageHealing = false;

	while (trying <= 0 || trying > 13) {
		cout << "enter trying of damage or healing: \n\n";
		cout << "max number of trying is 13: \n\n";
		cin >> trying;
	}
	cout << "good number!";
	
	
	vector<float> damageHealingArr = damageHealingBox(trying);
	



	
	displaySplashScreen();
	std::cout << "Loading complete. Showing statistics..." << std::endl;


	for (int i = 0; i < damageHealingArr.size(); ++i) {
		cout << damageHealingArr[i] << " ";
	}
	cout << std::endl;

	
	max = findBiggestPositiveIndex(damageHealingArr);
	cout << "the number of max damage is: \n\n";
	cout << max;
	cout << "\n\n";

	

	min = findSmallestPositiveIndex(damageHealingArr);
	cout << "the number of min damage is: \n\n";
	cout << min;
	cout << "\n\n";

	

	vector<size_t> healingArr = getNegativeIndices(damageHealingArr);
	cout << "healings numbers are: \n\n";

	for (int i = 0; i < healingArr.size(); ++i) {
		cout << healingArr[i] << " ";
	}
	cout << std::endl;
	cout << "\n\n";

	
	bool chooseChange = false;
	typeOfChange change = typeOfChange::damage;
	cout << "choose change what you want to know: 1 -- damage, 2 -- healing \n\n";
	int intChange = 0;
	float allResultChoose;
	while (!chooseChange) {
		cin >> intChange;
		if (intChange == 1 || intChange == 2) {
			chooseChange = true;

			switch (intChange) {
			case 1: {
				change = damage;
				cout << "you choose to know about damage \n";
				break;
			};
			case 2: {
				change = healing;
				cout << "you choose to know about healing \n";
				break;
			};
			default: {
				change = zero;
			}
			}
		}
		else {
			cout << "choose change what you want to know: 1 -- damage, 2 -- healing \n\n";
		}
	}

	allResultChoose = sumNumbers(damageHealingArr, change);
	if (change == 0) {
		cout << "all results of damage is: ";
		cout << allResultChoose;
		cout << "\n\n";
	}
	else {
		cout << "all results of healing is: ";
		cout << allResultChoose;
		cout << "\n\n";
	}

	
	vector<size_t> zeroArr = getZeroIndices(damageHealingArr);
	
	if (zeroArr.empty()) {
		std::cout << "there are no zero numbers!" << std::endl;
	}
	else {
		cout << "zero numbers are: \n\n";

		for (int i = 0; i < zeroArr.size(); ++i) {
			cout << zeroArr[i] << " ";
		}
		cout << std::endl;
		cout << "\n\n";
	}

	

	return 0;


}
*/
// --------------- lesson 13

/* 1.Написати програму, яка запитує і запам’ятовує, данні для створення персонажа грі:

Ім’я
Кількість здоров’я
Клас (один з двох на вибір: маг, воїн)


2.Доки в персонажа є здоров’я зчитувати значення шкоди, яку завдає персонажу ворог.

Якщо клас персонажа маг, то кожне парне число наносить подвійну шкоду від заданої
Якщо клас персонажа воїн, то кожне непарне число наносить потрійну шкоду, а парні не наносять шкоду


3.Завдання із зірочкою: 

Додати до персонажа параметр “потужність”
До попередніх умов додати наступні:


Якщо 
у воїна залишилося менше 30% здоров’я, 
то наносимий йому урон зменшується на величину параметру “потужність”

Якщо магу наноситься шкода більша за 2хпотужності, 
то є 50% ймовірності, що вона йому не зменшить здоров’я

*/

/* 
 
#include <iostream>
using namespace std;

enum heroClass {
	wizzard,
	warrior,
	joker
};

int main()
{
	// - hero init
	bool isDead = false;

	// - hero init -- health
	cout << "enter health: \n\n";
	float heroHealth = 100;
	cin >> heroHealth;

	// - hero init -- name
	cout << "enter your name: \n\n";
	string heroName = "Hero";
	cin >> heroName;

	// -- hero init -- class
	bool realHeroClass = false;
	heroClass chooseHeroClass = heroClass::wizzard;
	cout << "choose your class in game: 1 -- wizzard, 2 -- warrior \n\n";
	int intHeroClass = 0;
	while (!realHeroClass) {
		cin >> intHeroClass;
		if (intHeroClass == 1 || intHeroClass == 2) {
			realHeroClass = true;

			switch (intHeroClass) {
				case 1: {
					chooseHeroClass = wizzard;
					cout << "your class is wizzard \n";
					break;
				};
				case 2: {
					chooseHeroClass = warrior;
					cout << "your class is warrior \n";
					break;
				};
				default: {
					chooseHeroClass = joker;
				}
			}
		}
		else {
			cout << "choose your class in game: 1 -- wizzard, 2 -- warrior \n";
		}
	}

	// - hero init -- power
	int power = 0;
	bool powerLogic = false;
	while (!powerLogic) {
		cout << "set power from 1 to 10 \n";
		cin >> power;
		if (power <= 10 && power > 0) {
			powerLogic = true;
			cout << "power level: \n";
			cout << power;
			cout << "\n";
		}
	}


	// - start
	cout << "game is starting now \n";


	// -- damage
	int damage = 0;
	while (!isDead) {
		cout << "enter damage \n";
		cin >> damage;
		cout << "damage: \n";
		cout << damage;
		cout << "\n";

		if (chooseHeroClass == wizzard) {


			if (damage % 2 == 0) {
				heroHealth -= damage * 2;
			}
			else {
				heroHealth -= damage;
			}
		}
		else if (chooseHeroClass == warrior) {
			if (heroHealth <= 0.3 * heroHealth) {
				heroHealth -= power;
			}
			if (damage % 2 != 0) {
				heroHealth -= damage * 3;
			}
		}
		else {
			cout << "choose your class in game: 1 -- wizzard, 2 -- warrior \n\n";
		}
		if (heroHealth > 0) {
			cout << "health --> \n";
			cout << heroHealth;
			cout << "\n";
		}
		else {
			isDead = true;
			cout << "hero is dead \n";
			heroHealth = 0;
		}
		
	}

	return 0;
}

*/
